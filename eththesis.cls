% eththesis --- LaTeX class for ETH theses

% Copyright (C) 2007, 2008, 2010 
%
% This LaTeX class file is free software: you can redistribute it
% and/or modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation, either version 3 of
% the License, or (at your option) any later version.
% 
% eththesis.cls is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% The GNU General Public License is available at
% <http://www.gnu.org/licenses/>.

% eththesis.cls uses packages 
%  - geometry (general page layout)
%  - setspace (doublespace)
%  - titlesec (layout of section headers)
%  - titletoc (layout of table of contents)
%  - tocbibind (create entries in the toc for lof, lot, loa, and references)
%  - calc (for calculating dimensions)
%  - ulem (underlining)

\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{eththesis}[2010/10/16]

% options handled by report.cls
\DeclareOption{10pt} {\PassOptionsToClass{10pt}{report}}
\DeclareOption{11pt} {\PassOptionsToClass{11pt}{report}}
\DeclareOption{12pt} {\PassOptionsToClass{12pt}{report}}
\DeclareOption{oneside} {\PassOptionsToClass{oneside}{report}}
\DeclareOption{twoside} {\PassOptionsToClass{twoside}{report}}
\DeclareOption{draft} {\PassOptionsToClass{draft}{report}}
\DeclareOption{final} {\PassOptionsToClass{final}{report}}
\DeclareOption{openright} {\PassOptionsToClass{openright}{report}}
\DeclareOption{openany} {\PassOptionsToClass{openany}{report}}
\DeclareOption{leqno} {\PassOptionsToClass{leqno}{report}}
\DeclareOption{fleqn} {\PassOptionsToClass{fleqn}{report}}
\DeclareOption{openbib} {\PassOptionsToClass{openbib}{report}}

% line spacing handled by setspace

\newif\if@doublespacing \@doublespacingtrue % default
\DeclareOption{singlespacing} {\@doublespacingfalse
                               \PassOptionsToPackage{singlespacing}{setspace}}
\DeclareOption{onehalfspacing}{\@doublespacingfalse
                               \PassOptionsToPackage{onehalfspacing}{setspace}}
\DeclareOption{doublespacing} {\@doublespacingtrue
                               \PassOptionsToPackage{doublespacing}{setspace}}

\newif\if@normalem \@normalemtrue % default
\DeclareOption{ULforem} {\@normalemfalse
                         \PassOptionsToPackage{ULforem}{ulem}}
\DeclareOption{UWforbf} {\PassOptionsToPackage{UWforbf}{ulem}}

% relevant only with option openright:
% if a chapter ends with an odd page number, suppress header with page number
% on the following empty even page
\DeclareOption{emptyblank}{\def\cleardoublepage{%
  \clearpage\if@twoside \ifodd\c@page\else
  \hbox{}
  \vspace*{\fill}
  \thispagestyle{empty}
  \newpage
  \if@twocolumn\hbox{}\newpage\fi\fi\fi}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load the report class with certain options by default.
% It will process any other options specified.
\ProcessOptions
\LoadClass[a4paper,onecolumn]{report}[1996/10/31]

%%%%%%%% DEFINE UNDERLINING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Use ulem for section headers (but this package can do more if desired)
\if@normalem
  \RequirePackage[normalem]{ulem} % our default
\else
  \RequirePackage[ULforem]{ulem}  % ulem's default
\fi

%%%%%%% DEFINE DOUBLE-SPACING (THE DEFAULT) AND OTHER SPACING:
% setspace defines commands \singlespacing, \onehalfspacing, \doublespacing
% and environments singlespace, onehalfspace, doublespace.
% The spacing environment takes one argument which is the larger
% baselinestretch to use, e.g., \begin{spacing}{2.5}.
% Figure and table captions aer set single space.
\if@doublespacing
  \RequirePackage[doublespacing]{setspace} % our default
\else
  \RequirePackage[singlespacing]{setspace} % setspace's default
\fi

%%%%% SET THE OVERALL DOCUMENT PROPERTIES, such as page numbering,
% textwidth = 8.5in - 1.6in - 1.0in = 5.9in
\RequirePackage[a4paper, lmargin=1.6in, rmargin=1.0in, tmargin=1.0in,%
                bmargin=1.0in, ignorefoot, includehead, dvips %, showframe
                ]{geometry}
\RequirePackage{calc}

% These flags control whether we generate a list of figures (lof)
% and a list of tables (lot).
\newif\ifth@lof \th@loffalse
\newif\ifth@lot \th@lotfalse

\AtBeginDocument{
  \@ifundefined{th@loacount}{\gdef\th@loacount{0}}{}
  %
  \let\th@figure\figure
  \def\figure{%
    \ifth@lof\else
      \global\th@loftrue
      \immediate\write\@mainaux{\string\gdef\string\th@lofflag{}}
    \fi
    \th@figure}
  %
  \let\th@table\table
  \def\table{%
    \ifth@lot\else
      \global\th@lottrue
      \immediate\write\@mainaux{\string\gdef\string\th@lotflag{}}
    \fi
    \th@table}
  % hyperref compatibility
  \@ifpackageloaded{hyperref}{%
    \let\thMakeUppercase\relax
    \def\tocpdfbookmark{%
      \if@openright
       \cleardoublepage
      \else
       \clearpage
      \fi
      \pdfbookmark[0]{Table of Contents}{foo bar baz}}}
   {\let\thMakeUppercase\MakeUppercase
    \let\tocpdfbookmark\relax}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Use titlesec for formatting all the headlines
\RequirePackage[newlinetospace]{titlesec}
\titleformat{\chapter}[display]
{\bfseries\Large}
{\filleft{ \@chapapp\ \Huge\thechapter}}
{4ex}
{\titlerule
\vspace{2ex}%
\filright}
[\vspace{2ex}%
\titlerule]

%\titleformat{\chapter}[display] % top level 1
% {\thispagestyle{empty}%
%  \filcenter\large\bfseries}%
% {\@chapapp\ \thechapter}{0pt}{\MakeUppercase}
\titleformat{\section} % level 2
 {\large\bfseries}{\thesection}{1em}{}
\titleformat{\subsection} % level 3
 {\large\bfseries}{\thesubsection}{1em}{\uline}
\titleformat{\subsubsection}[hang] % level 4
 {\bfseries}{\thesubsubsection}{1em}{\uline}
\titleformat{\paragraph}[runin] % level 5
 {\bfseries}{\theparagraph}{1em}{\uline}[.]
\titleformat{\subparagraph}[runin] % level 6
 {\bfseries}{\thesubparagraph}{1em}{}[.]

\titlespacing{\chapter}{0pt}{1in-\headsep-\headheight
 -1.15\baselineskip}{*5} % top level 1
\titlespacing{\section}{0pt}{*5}{*5} % level 2
\titlespacing{\subsection}{0pt}{*5}{*5} % level 3
\titlespacing{\subsubsection}{0pt}{*5}{*5} % level 4
\titlespacing{\paragraph}{\parindent}{*2}{*1} % level 5
\titlespacing{\subparagraph}{\parindent}{*2}{*1} % level 6

% Use titletoc for formatting the table of contents, the lists of
% tables, figures, and appendices
\RequirePackage{titletoc}

\contentsmargin[0.6pc]{2.5em} % set right margin
\titlecontents
  {chapter} [1.5em] {}                  % {<section>} [left] {<above>}
  {\contentslabel{1.5em}\thMakeUppercase} % {<before with lablel>}
  {\hspace*{-1.5em}\thMakeUppercase}      % {<before without label>}
  {\titlerule*[0.6pc]{.}\contentspage}  % {<filler and page>}
% \dottedcontents{section}[<left>]{<above>}{<label width>}{<leader width>}
% The difference (<left> - <label width>) is <left> for the next-upper level
\dottedcontents{section}[3.8em]{}{2.3em}{0.6pc}
\dottedcontents{subsection}[7.0em]{}{3.2em}{0.6pc}
\dottedcontents{subsubsection}[11.1em]{}{4.1em}{0.6pc}
\dottedcontents{figure}[3.8em]{}{2.3em}{0.6pc}
\dottedcontents{table}[3.8em]{}{2.3em}{0.6pc}

\contentsuse{chapter}{loa} % needed by titletoc

% Use tocbibind to create entries in the toc for lof, lot, loa, and references.
% Suppress entry `toc' in toc.
\RequirePackage[nottoc]{tocbibind}
% \tocfile is command in tocbibind for new listings
\newcommand{\listofappend}{\tocfile{List of Appendices}{loa}}

% extend the definition of \appendix in report.cls
\def\appendix{\par
  \setcounter{chapter}{0}%
  \setcounter{section}{0}%
  \gdef\@chapapp{\appendixname}%
  \gdef\thechapter{\@Alph\c@chapter}%
  %
  \newpage
  % Only if there are several appendices, we make here
  % a generic entry "Appendices" in toc. Otherwise, we let
  % the redefined chapter command do this, so that it inserts
  % also the title.
  \ifnum\th@loacount>1
   \addcontentsline{toc}{chapter}{Appendices}%
  \fi
  %
  % header for appendices are vertically centered
  \ifnum\th@loacount>1%
    \titleformat{\chapter}[display]%
      {\vspace*{\fill}\thispagestyle{empty}%
       \singlespacing\filcenter\large\bfseries}%
      {\MakeUppercase{\@chapapp}\ \thechapter}{1em}{\MakeUppercase}%
      [\vspace*{\fill}\vspace*{8ex}\newpage]%
  \else
    \titleformat{\chapter}[display]%
      {\vspace*{\fill}\thispagestyle{empty}%
       \singlespacing\filcenter\large\bfseries}%
      {\MakeUppercase{\@chapapp}}{1em}{\MakeUppercase}%
      [\vspace*{\fill}\vspace*{8ex}\newpage]%
  \fi
  \titlespacing*{\chapter}{0pt}{*0}{*0}%
  %
  % If we have more than one appendix, they do not appear in the regular
  % table of contents, but in a separate list of appendices. So we
  % redefine \@chapter such that it writes into file loa instead of toc.
  % Code adapted from report.cls
  \def\@chapter[##1]##2{%
    \refstepcounter{chapter}%
    \typeout{\@chapapp\space\thechapter.}%
    \ifnum\th@loacount>1%
      \addcontentsline{loa}{chapter}%
                      {\protect\numberline{\thechapter}##1}%
    \else
      \addcontentsline{toc}{chapter}{Appendix: ##1}
    \fi
    \chaptermark{##1}%
    \addtocontents{lof}{\protect\addvspace{10\p@}}%
    \addtocontents{lot}{\protect\addvspace{10\p@}}%
    % Generate list of appendices only if we have at least two appendices.
    % Use \th@loacount for that.
    \immediate\write\@mainaux{\string\gdef\string\th@loacount{\arabic{chapter}}}%
    \@makechapterhead{##2}%
    \@afterheading}%
  %
  % adapted from titlesec.sty
  \def\ttl@addcontentsline##1##2{\ifnum\th@loacount>1%
   \addcontentsline{loa}{##1}{\ifttl@toclabel\ttl@a\fi##2}\fi}}

%%%%%%%%%%% ADDITIONAL FORMATTING STUFF:
\pagenumbering{arabic}
\pagestyle{myheadings} % no page headings
\onecolumn
\raggedbottom

% Suppress widows and orphans
\clubpenalty = 10000 
\widowpenalty = 10000 
\displaywidowpenalty = 10000 

\setcounter{tocdepth}{3}
\setcounter{secnumdepth}{3} % no numbering of subsubsections

% Set up some titles on the table of contents page.
\renewcommand*{\contentsname}{Table of Contents}
\renewcommand*{\bibname}{References}
\newcommand*{\listofappendname}{List of Appendices}

%%%%%%%%%% COMMANDS CHANGED FOR THIS CLASS
% Unavailable commands
\renewcommand*{\theindex}{\ClassWarning{eththesis}
  {Indexing not available in this class.}}
\renewcommand*{\twocolumn}{\ClassWarning{eththesis}
  {Twocolumn not available in this class.}}
\renewcommand*{\thepart}{\ClassWarning{eththesis}
  {``thepart'' not available in this class.}}
\renewcommand*{\part}{\ClassWarning{eththesis}
  {``part'' not available in this class.}}

%%%%%%%%%%% THESIS PROLOGUE PAGES:
% Default values of storage variables.  Emits an error message.
\newcommand*{\th@university}{Eidgen\"ossische Technische Hochschule Z\"urich}
\newcommand*{\th@uniabbv}{ETH Z\"urich}
\newcommand*{\th@address}{Z\"urich, Schweiz}
\newcommand*{\th@degreemonth}{\ifcase\month\or
  January\or February\or March\or April\or May\or June\or
  July\or August\or September\or October\or November\or December\fi}
\newcommand*{\th@degreeyear}{\number\year}

\newcommand*{\th@dissno}{
  \ClassError{eththesis}{
    You must specify a thesis title!}
  {Use the \protect\dissno{} command in the preamble.}}

\newcommand*{\th@title}{
  \ClassError{eththesis}{
    You must specify a thesis title!}
  {Use the \protect\title{} command in the preamble.}}

\newcommand*{\th@author}{
  \ClassError{eththesis}{
    You must specify your name!}
  {Use the \protect\author{}{} command in the \MessageBreak
    preamble. The first arg is your first name and \MessageBreak
    middle initial.  The second arg is your last name.}}

\newcommand*{\th@department}{
  \ClassError{eththesis}{
    You must specify the name of the department \MessageBreak
    granting your degree.}
  {Use the \protect\department{} command in the \MessageBreak
    preamble.}}

\newcommand*{\th@ETHdegree}{
  \ClassError{eththesis}{
    You must specify the type of degree you are receiving.}
  {Use the \protect\ETHdegree{}{}{}{} command in the preamble.\MessageBreak  
   The first arg is the thesis type, e.g., dissertation.\MessageBreak
   The second arg is the abbreviated degree, e.g., Ph.D.,\MessageBreak
   The third arg is the unabbreviated degree,\MessageBreak
   e.g. 'Doctor of Philosophy'.}}
\let\th@ETHdegree\th@abbrevdegree
\let\th@ETHdegree\th@thesis

\newcommand*{\th@currentdegree}{
  \ClassError{eththesis}{
    You must specify the type of degree you are receiving.}
  {Use the \protect\currentdegree{}{}command in the preamble.\MessageBreak  
   The first arg is the abbreviated degree, e.g., M.Sc.,\MessageBreak
   The third arg is the institution name,\MessageBreak
   e.g. 'Doctor of Philosophy'.}}
\let\th@currentdegree\th@currdegtitle
\let\th@currentdegree\th@currdeginst

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The base commands that make all of the prologue pages.
\newcommand*{\th@Titlepage}{
  \thispagestyle{empty}%
  % The setspace package is not too good with aligning text vertically.
  % The following should work.
  \begin{singlespace}
    \noindent
    \raisebox{0.25in}[\textheight]{\begin{minipage}[b]
       [\textheight + \headheight + \headsep - 0.5in]{5.5in}
               \centerline{\small{Diss. ETH No. \th@dissno}}
    \vfill
       \centerline{\MakeUppercase{\th@university}}
       %\centerline{\MakeUppercase{\th@address}}
       \vspace{4ex}
       \centerline{\MakeUppercase{\th@degreemonth\space\th@degreeyear}}
       \vfill
       \begin{doublespace}
         {\Large\bfseries\begin{center}
           \uppercase\expandafter{\th@title}
         \end{center}}
         \vfill
         \begin{center}
           \MakeUppercase{A thesis submitted to attain the degree of}\\
           \MakeUppercase{\th@ETHdegree}\\
           \th@abbrevdegree \space\MakeUppercase{\th@uniabbv}
         \end{center}
         \vfill 
         \centerline{\uppercase{presented by}}
         \begin{singlespace}
           \centerline{\MakeUppercase{\th@author}}
           \centerline{\copyright\space\th@degreeyear\space\th@author}
         \end{singlespace}
          \begin{singlespace}
          \centerline{\th@currdegtitle \space \th@currdeginst}
          \end{singlespace}
         \vfill
         \begin{singlespace}
         \centerline{\MakeUppercase{born on \th@dob}}
         \end{singlespace}
         \vfill
         \begin{singlespace}
         \centerline{\MakeUppercase{citizen of}}
         \centerline{\MakeUppercase{\th@citizenship}}
         \end{singlespace}
         \vfill
         %\centerline{\MakeUppercase{\th@department}} 
       \end{doublespace}
       \vfill
       \hspace*{0in}\th@thesis\ accepted on the recommendation of: \\
       \hspace*{0.3in} \th@director, Director \\
       \hspace*{0.3in} \th@committeeone \\
       \hspace*{0.3in} \th@committeetwo
     \end{minipage}}
  \end{singlespace}
  \if@openright
   \cleardoublepage
  \else
   \clearpage
  \fi}
  
\newcommand{\th@Abstractpage}{%
  % The Abstract page has a left margin of 1.5in
  % (as compared to 1.6in in the rest of the document)
  {\addtolength{\oddsidemargin}{-0.1in}
   \addtolength{\evensidemargin}{-0.1in}
   \addtolength{\linewidth}{0.1in}
   \addtolength{\textwidth}{0.1in}
   \addtolength{\columnwidth}{0.1in}
   \setlength{\hsize}{\linewidth}
   % no head line for abstract
   \addtolength{\textheight}{\headheight + \headsep}
   \setlength{\headheight}{0pt}
   \setlength{\headsep}{0pt}
   \pagestyle{empty}%
   \begin{singlespace}
     % \vspace is strange:
     % On this page, we want the top margin to be 1.5in.
     % But we only add 0.25in to the default 1.0in to get exactly
     % what we want.
     \vspace*{0.25in}
     \centerline{\bfseries ABSTRACT}
     \vspace*{0.5in}
     {\bfseries\begin{center}
        \uppercase\expandafter{\th@title}
      \end{center}}
     \vspace{1ex}
     \begin{center}
       \th@author\\
       \th@department\\
       \th@university, \th@degreeyear\\
       \th@director, Director
     \end{center}
     \vspace*{0.5in}
     \begin{doublespace}
       \unvbox\absbox
     \end{doublespace}
   \end{singlespace}
   \if@openright
    \cleardoublepage
   \else
    \clearpage
   \fi}%
    \pagestyle{myheadings}} % no page headings

   \newcommand{\th@AbstractpageDE}{%
  % The Abstract page has a left margin of 1.5in
  % (as compared to 1.6in in the rest of the document)
  {\addtolength{\oddsidemargin}{-0.1in}
   \addtolength{\evensidemargin}{-0.1in}
   \addtolength{\linewidth}{0.1in}
   \addtolength{\textwidth}{0.1in}
   \addtolength{\columnwidth}{0.1in}
   \setlength{\hsize}{\linewidth}
   % no head line for abstract
   \addtolength{\textheight}{\headheight + \headsep}
   \setlength{\headheight}{0pt}
   \setlength{\headsep}{0pt}
   \pagestyle{empty}%
   \begin{singlespace}
     % \vspace is strange:
     % On this page, we want the top margin to be 1.5in.
     % But we only add 0.25in to the default 1.0in to get exactly
     % what we want.
     \vspace*{0.25in}
     \centerline{\bfseries ZUSAMMENFASSUNG}
     \vspace*{0.5in}
     {\bfseries\begin{center}
        \uppercase\expandafter{\th@title}
      \end{center}}
     \vspace{1ex}
     \begin{center}
       \th@author\\
       \th@department\\
       \th@university, \th@degreeyear\\
       \th@director, Director
     \end{center}
     \vspace*{0.5in}
     \begin{doublespace}
       \unvbox\absdebox
     \end{doublespace}
   \end{singlespace}
   \if@openright
    \cleardoublepage
   \else
    \clearpage
   \fi}%
   
 \pagestyle{myheadings}} % no page headings

\newcommand{\th@Dedicationpage}{
  \ifhbox\dedibox
   \thispagestyle{empty}
   % centered
   \noindent\hspace*{-0.1in}
   \begin{minipage}{5.5in}
     \vspace*{\topskip}
     \chapter*{Dedication}
     \begin{center}
       \unhbox\dedibox
     \end{center}
   \end{minipage}
   \if@openright
    \cleardoublepage
   \else
    \clearpage
   \fi
   \fi}

\newcommand{\th@Acknowledgementpage}{
  \ifvbox\acknbox
   \thispagestyle{empty}
   \chapter*{Acknowledgements}
   \unvbox\acknbox
   \if@openright
    \cleardoublepage
   \else
    \clearpage
   \fi
  \fi}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\def\th@renewlox#1#2#3#4{%
  \renewcommand*{#1}{%
    \chapter*{#2}
    % All lox except toc get an entry in toc.
    \ifx#1\tableofcontents\else
      \addcontentsline{toc}{chapter}{#2}%
    \fi
    \begin{singlespace}%
    \protect\sloppy
      % Hack: we add the `Page' header line by extending
      % the material that goes into the \output routine
      % (inspired by afterpage.sty).
      \newtoks\th@output
      \th@output\expandafter{\the\output}
      \output{\the\th@output
       \leftskip=0pt \noindent\hbox to\textwidth{#4\hfil Page}\par}%
      \vspace{-0.70\baselineskip}%
      \parskip 0.70\baselineskip
      \@starttoc{#3}%
    \end{singlespace}}}

% Redefinition of the \listofxxx commands
\th@renewlox{\tableofcontents}{\contentsname}{toc}{Chapter}
\th@renewlox{\listoftables}{\listtablename}{lot}{Table}
\th@renewlox{\listoffigures}{\listfigurename}{lof}{Figure}
\th@renewlox{\listofappend}{\listofappendname}{loa}{Appendix}

\def\th@loxheader#1#2{%
 \par\penalty-100
 \addtocontents{#1}{\addpenalty{\@secpenalty}\noindent #2\hfill Page\par
  \penalty1000}}

% Use these commands for the appropriate headers on the second /
% third etc. page of a table of contents / list of figures etc.
\newcommand*{\tocheader}{\th@loxheader{toc}{Chapter}}
\newcommand*{\lotheader}{\th@loxheader{lot}{Table}}
\newcommand*{\lofheader}{\th@loxheader{lof}{Figure}}
\newcommand*{\loaheader}{\th@loxheader{loa}{Appendix}}

%%%%%%%%% USER COMMANDS FOR CONTROLLING THE PROLOGUE:
\renewcommand*{\title}[1]{\gdef\th@title{#1}}
\newcommand*{\ETHdegreedate}[2]{%
 \gdef\th@degreemonth{#1}
 \gdef\th@degreeyear{#2}}
\renewcommand*{\author}[1]{%
  \gdef\th@author{#1}}
\newcommand*{\ETHdegree}[3]{%
  \gdef\th@thesis{#1}%
  \gdef\th@abbrevdegree{#2}%
  \gdef\th@ETHdegree{#3}}
\newcommand*{\director}[1]{%
  \gdef\th@director{#1}}
\newcommand*{\department}[1]{%
  \gdef\th@department{#1}}
\newcommand*{\dissno}[1]{%
  \gdef\th@dissno{#1}}
\newcommand*{\committee}[2]{%
	\gdef\th@committeeone{#1}
	\gdef\th@committeetwo{#2}}
\newcommand*{\citizenship}[1]{%
	\gdef\th@citizenship{#1}}
\newcommand*{\dob}[1]{%
	\gdef\th@dob{#1}}
\newcommand*{\currentdegree}[2]{%
  	\gdef\th@currdegtitle{#1}%
 	\gdef\th@currdeginst{#2}}
  
\newbox\absbox
\renewenvironment{abstract}{%
  \setlength{\hsize}{6.0in}
  \global\setbox\absbox\vbox\bgroup
  \color@begingroup}%
 {\par\unskip
  \color@endgroup
  \egroup}
  
  \newbox\absdebox
  \newenvironment{abstractde}{%
  \setlength{\hsize}{6.0in}
  \global\setbox\absdebox\vbox\bgroup
  \color@begingroup}%
 {\par\unskip
  \color@endgroup
  \egroup}

\newbox\dedibox
\newenvironment{dedication}{%
 % dedication goes cenetere
  \global\setbox\dedibox\hbox\bgroup
  \color@begingroup}%
 {\par\unskip
  \color@endgroup
  \egroup}

\newbox\acknbox
\newenvironment{acknowledgements}{%
  \global\setbox\acknbox\vbox\bgroup
  \color@begingroup}%
 {\par\unskip
  \color@endgroup
  \egroup}
% Which spelling is correct?
\let\acknowledgments\acknowledgements
\let\endacknowledgments\endacknowledgements

\def\lox@header#1#2{%
 \par\addtocontents{#1}{\noindent #2\hfill Page\par}}

% Here we put everything together to generate the prologue
\newcommand{\MakeThesisPrologue}{
  \th@Titlepage
  \th@Abstractpage
  \th@AbstractpageDE
  \pagenumbering{roman}
  \th@Acknowledgementpage
  \th@Dedicationpage
  %
  \newif\ifth@loxheaderflag \th@loxheaderflagfalse
  \@ifundefined{th@lotflag}{}{\th@loxheaderflagtrue}
  \@ifundefined{th@lofflag}{}{\th@loxheaderflagtrue}
  \ifnum\th@loacount>1 \th@loxheaderflagtrue\fi
  %
  \ifth@loxheaderflag
    \addtocontents{toc}{\hspace*{\fill}Page\par}
    \lox@header{lot}{Table}
    \lox@header{lof}{Figure}
    \lox@header{loa}{Appendix}
  \else
    \lox@header{toc}{Chapter}
  \fi
  % ToC Page
  \tocpdfbookmark
  \tableofcontents
  % LoT Page
  \@ifundefined{th@lotflag}{}{\listoftables}
  % LoF Page
  \@ifundefined{th@lofflag}{}{\listoffigures}
  % LoA Page
  \ifnum\th@loacount>1 \listofappend\fi
  %
  % \listofXXX is a hook dummy to add more stuff such as a List of Symbols
  % Enable it by using something like \newcommand{\listofXXX}{\input{symbols}}
  \@ifundefined{listofXXX}{}{\listofXXX}
  %
  \ifth@loxheaderflag 
    \addtocontents{toc}{\noindent Chapter\par} 
  \fi
  %
  \if@openright
    \cleardoublepage
  \else
    \clearpage
  \fi
  \pagenumbering{arabic}}

\endinput
%%
%%%%%%%%%%%%%%%% End of file ``eththesis.cls''

%%% Local Variables: 
%%% TeX-master: "~/tex/inputs/packages/eththesis/example/mythesis"
%%% End: 
